#Christians first pysql attempts
import sqlite3,numpy

def read_db(dbase):
    connect=sqlite3.connect(dbase)
    curse=connect.cursor()
    return curse
"""Tests ignore:
def qstr1():
    a="select distinct releases.substance_name from releases"
    return a

def qstr2():
    b="""select facilities.name as 'LPS',facilities.administrative_number AS 'E-RTR/PRTR Number',facilities.wgs84_x as 'Long',facilities.wgs84_y as 'Lat',releases.annual_load/1000000 'NOx t' \
    from facilities inner join releases ON releases.id = facilities.id where releases.substance_name='Nitrogen oxides (NOx/NO2)'"""
    return b"""

def qstr3_gen(substance,factor):
    c="""select facilities.id,facilities.name as 'LPS',facilities.administrative_number AS 'E-RTR/PRTR Number',facilities.wgs84_x as 'Long',facilities.wgs84_y as 'Lat',releases.annual_load/"""+factor+""" \
        from facilities inner join releases ON releases.id = facilities.id where releases.substance_name="""+'\''+substance+'\''
    return c
## get the factories with their Ids
def factories():
    c="""select\
    facilities.id,facilities.name as 'LPS', facilities.administrative_number AS\
    'E-RTR/PRTR Number', facilities.wgs84_x as 'Long', facilities.wgs84_y as 'Lat'\
    from releases inner join facilities ON releases.id = facilities.id"""
    return c

def querry(qstring,curse):
    dt=curse.execute(qstring+';')
    dat=dt.fetchall()
    return dat
"""" Only tests please ignore
def doit(dbase):
    datab=read_db(dbase)
    sql1 = qstr1()
    data1=querry(sql1,datab)
    sql2=qstr2()
    data2=querry(sql2,datab)
    return data1,data2
#run this! for the exercise pt1

def result_clean(res):
    L=[]
    for i in res:
        n=i[-1]
        L.append(n)
    return L
# Coordinaten Hashen
def hashcoord(dliste,x=-1,y=-2):
    L=[]
    for i in enumerate(dliste):
        hsh=hash(str(i[1][x])+str(i[1][y]))
        L.append(list(i[1])+[hsh])
    return L

def hashfid(dliste,x=0):
    L=[]
    for i in enumerate(dliste):
        hsh=hash(str(i[1][x]))#ohne strip
        L.append(list(i[1])+[hsh])
    return L

def hashfid2(dliste,x=0):
    L=[]
    for i in enumerate(dliste):
        hsh=i[1][x]#ohne strip
        L.append(list(i[1])+[hsh])
    return L
"""
#hier die Basisliste bauen
def doit2_factory(dbase):
    datab = read_db(dbase)
    qs=factories()
    datah=querry(qs,datab)
    hashbasisliste=hashfid2(datah,x=0)#gehashte Basisliste
    hashbasisliste=numpy.asarray(hashbasisliste)
    return datah,hashbasisliste
#hier alle querries mit den faktoren
def doit2(dbase):
    datab = read_db(dbase)
    searchvector = 'Nitrogen', 'NMVOC', 'Sulphur oxides', 'Ammonia', 'Particulate matter (PM10)', 'Carbon monoxide', 'Lead', 'Cadmium', 'Mercury', 'PCD +', "Polycyclic aromatic", 'HCB', 'PCBs'
    factor=1000000,1000000,1000000,1000000,1000000,1000000,1000,1000,1000,1,1000,1,1
    sql1 = qstr1()
    data1 = querry(sql1, datab)
    L=[]
    for i in searchvector:
        for j in data1:
            try:
                if i in j[0]:
                    L.append(j[0])
            except(Exception):
                continue
    subndarr=[]
    for b in enumerate(L):
        result=qstr3_gen(b[1],str(factor[b[0]]))
        #print(result)
        #return result
        result1=querry(result,datab)
        #rescl=result_clean(result1)
        result1=hashfid2(result1,x=0)
        rescl=numpy.asarray(result1)
        subndarr.append(rescl)
    return data1,subndarr#, data2
#Diese Funktion baut dann die Datentabelle auf
def create_table(dbase):
    a,b=doit2_factory(dbase)
    x,y=doit2(dbase)
    factkeys=b[:,-1]
    factkeys=factkeys.astype(float)
    L=[]
    for j in enumerate(factkeys):
        g=[]
        for i in enumerate(y):
            vec=i[1][:,-1].astype(float)
            index=numpy.where(vec==j[1])[0]
            if len(index)==0:
                g.append(numpy.nan)
            elif len(index)!=0:
                g.append(i[1][index,-2][0])
        L.append(list(a[j[0]][1:-1])+g)
    return L

