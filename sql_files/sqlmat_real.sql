--New Table From 
CREATE TABLE tab_sectors AS
  SELECT activities.facility_id,activities.business_sector
  FROM activities;
--Update
Update tab_sectors
set business_sector='B_Industry'
where business_sector like '%industry%';
--Update 2
Update tab_sectors
set business_sector='A_PublicPower'
where business_sector like '%Energy%';
--Update 3
Update tab_sectors
set business_sector='J_Waste'
where business_sector like '%Waste%';
--Update 4
Update tab_sectors
set business_sector='K_AgriLivestock'
where business_sector like '%livestock%';
--Update 5
Update tab_sectors
set business_sector='L_AgriOther'
where business_sector like '%agricult%';
--Create PRTR Table1
CREATE TABLE prtr_front AS
select facilities.id,releases.year,facilities.name as 'LPS',tab_sectors.business_sector as 'GNFR',facilities.administrative_number AS 'Facility_PRTR_ID',facilities.wgs84_x as 'Longitude',facilities.wgs84_y as 'Latitude'
from facilities 
left join releases ON facilities.id=releases.facility_id 
left join tab_sectors ON facilities.id=tab_sectors.facility_id;
--Cleanup
Drop Table tab_sectors;
--Create NOx tab
CREATE TABLE noxx AS
select releases.facility_id,releases.annual_load/1000000 AS 'NOx_as_NO2'
from releases
where releases.substance_name LIKE '%Nitrogen%';
--Create NMVOC tab
CREATE TABLE nmvoc AS
select releases.facility_id,releases.annual_load/1000000 AS 'NMVOC'
from releases
where releases.substance_name LIKE '%NMVOC%';
--Create sulphur tab
CREATE TABLE sulphur AS
select releases.facility_id,releases.annual_load/1000000 AS 'SOx_as_SO2'
from releases
where releases.substance_name LIKE '%Sulphur oxides%';
--Create amonia tab
CREATE TABLE nh3 AS
select releases.facility_id,releases.annual_load/1000000 AS 'NH3'
from releases
where releases.substance_name LIKE '%NH3%';
----Create PM25
CREATE TABLE pm25 AS
select releases.facility_id,releases.annual_load/1000000 AS 'PM25'
from releases
where releases.substance_name LIKE '%PM25%';
----Create PM10
CREATE TABLE pm10 AS
select releases.facility_id,releases.annual_load/1000000 AS 'PM10'
from releases
where releases.substance_name LIKE '%PM10%';
----Create CO
CREATE TABLE co AS
select releases.facility_id,releases.annual_load/1000000 AS 'CO'
from releases
where releases.substance_name LIKE '%CO%';
----Create Pb
CREATE TABLE pb AS
select releases.facility_id,releases.annual_load/1000 AS 'Pb'
from releases
where releases.substance_name LIKE '%Pb%';
----Create Cd
CREATE TABLE cd AS
select releases.facility_id,releases.annual_load/1000 AS 'Cd'
from releases
where releases.substance_name LIKE '%Cd%' and releases.year=2018;
----Create Hg
CREATE TABLE hg AS
select releases.facility_id,releases.annual_load/1000 AS 'Hg'
from releases
where releases.substance_name LIKE '%Hg%';
----Create furan
CREATE TABLE furan AS
select releases.facility_id,releases.annual_load/0.001 AS 'PCDD_PCDF_dioxins_furans'
from releases
where releases.substance_name LIKE '%furans%';
--create PAHs
CREATE TABLE pah AS
select releases.facility_id,releases.annual_load/1000 AS 'PAHs'
from releases
where releases.substance_name LIKE '%PAHs%';
--create HCB
CREATE TABLE hcb AS
select releases.facility_id,releases.annual_load AS 'HCB'
from releases
where releases.substance_name LIKE '%HCB%';
--create PCB
CREATE TABLE pcb AS
select releases.facility_id,releases.annual_load AS 'PCBs'
from releases
where releases.substance_name LIKE '%PCB%';
--create final table for 2015
CREATE TABLE fint AS
select prtr_front.LPS,prtr_front.GNFR,prtr_front.Facility_PRTR_ID,prtr_front.Longitude,prtr_front.Latitude,noxx.NOx_as_NO2,nmvoc.NMVOC,
sulphur.SOx_as_SO2,nh3.nh3,pm25,pm10.PM10,co.CO,pb.Pb,hg.Hg,furan.PCDD_PCDF_dioxins_furans,pah.PAHs,hcb.HCB,pcb.PCBs
from prtr_front
left join noxx ON prtr_front.id=noxx.facility_id 
left join nmvoc ON prtr_front.id=nmvoc.facility_id
left join sulphur ON prtr_front.id=sulphur.facility_id
left join nh3 ON prtr_front.id=nh3.facility_id
left join pm25 ON prtr_front.id=pm25.facility_id
left join pm10 ON prtr_front.id=pm10.facility_id
left join co ON prtr_front.id=co.facility_id
left join pb ON prtr_front.id=pb.facility_id
left join cd ON prtr_front.id=cd.facility_id
left join hg ON prtr_front.id=hg.facility_id
left join furan ON prtr_front.id=furan.facility_id
left join pah ON prtr_front.id=pah.facility_id
left join hcb ON prtr_front.id=hcb.facility_id
left join pcb ON prtr_front.id=pcb.facility_id
where prtr_front.year=2015
group by prtr_front.Facility_PRTR_ID
order by prtr_front.LPS;

